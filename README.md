# Django Devops

## Projeto escrito em Django para disciplina de devops da Residência do TST em parceria com a UFRN

- Há 5 etapas no pipeline: linting, migrations, django-tests, package-job e deploy
- No linting é usado a biblioteca Black
- Em migrations o banco é atualizado
- Em django-tests roda os testes
- Em package-job é criado uma imagem Docker que é hospeada no GitLab
- Em deploy é feito deploy em um cluster no Google Cloud Platform
- É possível acessar o site em: http://35.198.59.136:8000/
